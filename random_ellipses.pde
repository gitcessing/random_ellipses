/**
* Draws random ellipses on the screen
*/
void setup(){
   size(800, 600);
}


void draw(){
   ellipse(random(width), random(height), random(10, 20), random(10,20));
}
